%
% Initialisation des variables
%

%% Initialisation des variables
Temps_Sim = 10;     % Temps en secondes
Step_Sim = 0.01;    % Step de simulation
Time_Sim = [0:Step_Sim:Temps_Sim]';
k=0.5;              % Gain du mod�le
Signal = [zeros((Temps_Sim * 0.1)/Step_Sim+1,1);ones((Temps_Sim * 0.9)/Step_Sim,1)];   