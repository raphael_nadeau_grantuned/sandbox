%
%
% Petit script pour caller le simulink et sortir un graphique.
%
% Auteur: R.Nadeau
%

%% Initialisation de l'environnement
clear all
close all
clc

%% Initialisation des variables
Init_variables



%% Appel de la simulation
simin = [Time_Sim,Signal];   % Variable de simulation
Sim_Out = sim('main');

%% Affichage des r�sultats
plot(Time_Sim,simout)
title('R�ponse � l''echelon d''un modele simple (Sandbox)')
xlabel('Temps (s)')
ylabel(['Value, gain k = ' num2str(k)])